import Vue from 'vue';
import Router from 'vue-router';
import ChoixClasse from '@/components/ChoixClasse';
import FaireAppel from '@/components/FaireAppel';
import AppelFini from '@/components/AppelFini';

Vue.use(Router);

export default new Router({
  routes: [
    { path: '/', name: 'ChoixClasse', component: ChoixClasse },
    { path: '/classe/:classe', name: 'FaireAppel', component: FaireAppel },
    { path: '/appelFini', name: 'AppelFini', component: AppelFini },
  ],
});
