package com.service;

import com.model.*;
import com.repository.ReferentielRepository;
import com.utils.FormatFichierIncorrect;

import java.util.Collections;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class RecupererAppelClasse {

    private final ReferentielRepository referentielRepository;

    public RecupererAppelClasse(ReferentielRepository referentielRepository) {

        this.referentielRepository = referentielRepository;
    }

    public AppelClasse execute(String nomClasse) {
        return referentielRepository.recupererToutLeReferentiel().stream()
                                    .filter(recupererNiveauParClasse(nomClasse))
                                    .map(recupererAppelClasseParNiveau(nomClasse))
                                    .findFirst()
                                    .orElseThrow(FormatFichierIncorrect::new);
    }

    private Function<ReferentielNiveau, AppelClasse> recupererAppelClasseParNiveau(String nomClasse) {
        return referentielNiveau -> AppelClasse.builder()
                                               .appelEleves(buildAppelEleves(referentielNiveau, nomClasse))
                                               .build();
    }

    private List<AppelEleve> buildAppelEleves(ReferentielNiveau referentielNiveau, String nomClasse) {
        Classe classeAppel = referentielNiveau.getClasses().stream()
                                              .filter(classe -> classe.getNom().equals(nomClasse))
                                              .findFirst()
                                              .orElseThrow(FormatFichierIncorrect::new);

        List<QuestionReponse> questionReponsesAleatoires = triAleatoirementQuestionsReponses(referentielNiveau);

        return IntStream.range(0, classeAppel.getEleves().size())
                        .boxed()
                        .map(index -> AppelEleve.builder()
                                                .eleve(classeAppel.getEleves().get(index))
                                                .questionReponse(questionReponsesAleatoires.get(index))
                                                .build())
                        .collect(Collectors.toList());
    }

    private List<QuestionReponse> triAleatoirementQuestionsReponses(ReferentielNiveau referentielNiveau) {
        List<QuestionReponse> questionReponses = referentielNiveau.getQuestionReponses();
        Collections.shuffle(questionReponses);
        return questionReponses;
    }

    private Predicate<ReferentielNiveau> recupererNiveauParClasse(String nomClasse) {
        return referentielNiveau -> referentielNiveau.getClasses()
                                                     .stream()
                                                     .anyMatch(classe -> classe.getNom().equals(nomClasse));
    }
}
