package com.service;

import com.model.Classe;
import com.repository.ReferentielRepository;

import java.util.List;
import java.util.stream.Collectors;

public class RecupererClasses {

    private final ReferentielRepository referentielRepository;

    public RecupererClasses(ReferentielRepository referentielRepository) {
        this.referentielRepository = referentielRepository;
    }

    public List<String> execute() {
        return referentielRepository.recupererToutLeReferentiel()
                                    .stream()
                                    .flatMap(appel -> appel.getClasses().stream())
                                    .map(Classe::getNom)
                                    .sorted()
                                    .collect(Collectors.toList());
    }
}
