package com.repository;

import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.model.ValueRange;
import com.model.ReferentielNiveau;
import com.utils.Google;
import com.utils.GoogleException;

import java.io.IOException;
import java.util.List;

import static com.google.common.collect.Lists.newArrayList;

public class ReferentielRepository {

    private final Google google;
    private final CreerReferentielsParNiveau creerReferentielParNiveau;

    private static final String SPREADSHEET_ID = "1pRo7EWEB0sVKBgggkCdi4dUMoabHp_-G1urXtxxdeCQ";

    private static final String RANGE_3EME = "3ème";
    private static final String RANGE_4EME = "4ème";
    private static final String RANGE_5EME = "5ème";
    private static final String RANGE_6EME = "6ème";

    public ReferentielRepository(Google google, CreerReferentielsParNiveau creerReferentielParNiveau) {
        this.google = google;
        this.creerReferentielParNiveau = creerReferentielParNiveau;
    }

    public List<ReferentielNiveau> recupererToutLeReferentiel() {
        Sheets feuilles = google.recupererConnecteurSheet();
        try {
            return newArrayList(
                    recupererReferentielParNiveau(feuilles, RANGE_3EME),
                    recupererReferentielParNiveau(feuilles, RANGE_4EME),
                    recupererReferentielParNiveau(feuilles, RANGE_5EME),
                    recupererReferentielParNiveau(feuilles, RANGE_6EME)
            );
        } catch (IOException e) {
            throw new GoogleException(e);
        }
    }

    private ReferentielNiveau recupererReferentielParNiveau(Sheets feuilles, String annotationRange) throws IOException {
        ValueRange lignesRemplies = recupererLignesRemplies(feuilles, annotationRange);
        return creerReferentielParNiveau.execute(lignesRemplies);
    }

    private ValueRange recupererLignesRemplies(Sheets sheets, String annotationRange) throws IOException {
        return sheets.spreadsheets()
                .values()
                .get(SPREADSHEET_ID, annotationRange)
                .execute();
    }
}
