package com.repository;

import com.google.api.services.sheets.v4.model.ValueRange;
import com.model.ReferentielNiveau;
import com.model.Classe;
import com.model.Eleve;
import com.model.QuestionReponse;
import com.utils.FormatFichierIncorrect;

import java.util.Comparator;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static com.google.common.collect.Lists.newArrayList;
import static java.util.stream.Collectors.toList;

public class CreerReferentielsParNiveau {

    public ReferentielNiveau execute(ValueRange lignesRemplies) {
        List<QuestionReponse> questionReponses = recupererQuestionsReponses(lignesRemplies);
        List<Classe> classesSansEleve = recupererClasses(lignesRemplies);
        List<Classe> classesAvecEleve = remplirClasseAvecEleves(classesSansEleve, lignesRemplies);
        return construireAppel(questionReponses, classesAvecEleve);
    }

    private List<Classe> remplirClasseAvecEleves(List<Classe> classes, ValueRange lignesRemplies) {
        List<List<Eleve>> elevesSansClasse = recupererElevesSansClasse(lignesRemplies);
        return IntStream.range(0, classes.size())
                        .boxed()
                        .map(remplirClasseAvecElevesPossiblementVide(classes, elevesSansClasse))
                        .map(retirerEleveVideDesClasses())
                        .collect(toList());
    }

    private Function<Classe, Classe> retirerEleveVideDesClasses() {
        return classe -> classe.toBuilder()
                               .eleves(recupererElevesNonVide(classe))
                               .build();
    }

    private List<Eleve> recupererElevesNonVide(Classe classe) {
        return classe.getEleves().stream()
                     .filter(eleve -> !eleve.getNom().isEmpty())
                     .sorted(Comparator.comparing(Eleve::getNom))
                     .collect(Collectors.toList());
    }

    private Function<Integer, Classe> remplirClasseAvecElevesPossiblementVide(List<Classe> classes,
                                                                              List<List<Eleve>> elevesSansClasse) {
        return index -> classes.get(index).toBuilder()
                               .eleves(recupererElevesAvecIndex(elevesSansClasse, index))
                               .build();
    }

    private List<Eleve> recupererElevesAvecIndex(List<List<Eleve>> elevesSansClasse, Integer index) {
        return elevesSansClasse.stream()
                               .filter(eleves -> eleves.size() > index)
                               .map(eleves -> eleves.get(index))
                               .collect(toList());
    }

    private List<List<Eleve>> recupererElevesSansClasse(ValueRange lignesRemplies) {
        return lignesRemplies.getValues().stream()
                             .sequential()
                             .skip(1)
                             .map(recupererEleves())
                             .collect(toList());
    }

    private Function<List<Object>, List<Eleve>> recupererEleves() {
        return ligne -> ligne.stream()
                             .sequential()
                             .skip(2)
                             .map(creerEleve())
                             .collect(toList());
    }

    private Function<Object, Eleve> creerEleve() {
        return cellule -> Eleve.builder().nom((String) cellule).build();
    }

    private List<Classe> recupererClasses(ValueRange lignesRemplies) {
        return lignesRemplies.getValues().stream()
                             .sequential()
                             .findFirst()
                             .map(recupererClasses())
                             .map(construireClasses())
                             .orElseThrow(FormatFichierIncorrect::new);
    }

    private Function<List<Object>, List<Classe>> construireClasses() {
        return cellules -> cellules.stream()
                                   .map(construireClasse())
                                   .collect(toList());
    }

    private Function<Object, Classe> construireClasse() {
        return cellule -> Classe.builder()
                                .nom((String) cellule)
                                .build();
    }

    private Function<List<Object>, List<Object>> recupererClasses() {
        return list -> list.stream().skip(2).collect(toList());
    }

    private List<QuestionReponse> recupererQuestionsReponses(ValueRange lignesRemplies) {
        return lignesRemplies.getValues().stream()
                             .sequential()
                             .map(recupererDeuxPremiersElements())
                             .map(this::construireQuestionReponse)
                             .filter(recupererQuestionsNonVides())
                             .skip(1)
                             .collect(toList());
    }

    private Predicate<QuestionReponse> recupererQuestionsNonVides() {
        return questionReponse -> !questionReponse.getQuestion().isEmpty();
    }

    private ReferentielNiveau construireAppel(List<QuestionReponse> questionReponses, List<Classe> classes) {
        return ReferentielNiveau.builder()
                                .questionReponses(questionReponses)
                                .classes(classes)
                                .build();
    }

    private QuestionReponse construireQuestionReponse(List<Object> cellules) {
        return QuestionReponse.builder()
                              .question((String) cellules.get(0))
                              .reponse((String) cellules.get(1))
                              .build();
    }

    private Function<List<Object>, List<Object>> recupererDeuxPremiersElements() {
        return ligne -> ligne.subList(0, 2);
    }
}