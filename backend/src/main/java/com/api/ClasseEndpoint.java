package com.api;

import com.model.AppelClasse;
import com.repository.ReferentielRepository;
import com.repository.CreerReferentielsParNiveau;
import com.service.RecupererAppelClasse;
import com.service.RecupererClasses;
import com.utils.Google;
import com.utils.GoogleCredentialSupplier;

import java.util.List;

public class ClasseEndpoint {
    public static List<String> recupererClasses() {
        return initRecupererClasses().execute();
    }

    private static RecupererClasses initRecupererClasses() {
        return new RecupererClasses(
                new ReferentielRepository(
                        new Google(new GoogleCredentialSupplier()),
                        new CreerReferentielsParNiveau())
        );
    }

    public static RecupererAppelClasse initRecupererAppelClasse() {
        return new RecupererAppelClasse(
                new ReferentielRepository(
                        new Google(new GoogleCredentialSupplier()),
                        new CreerReferentielsParNiveau()));
    }

    public static AppelClasse recupererClasse(String nomClasse) {
        return initRecupererAppelClasse().execute(nomClasse);
    }
}
