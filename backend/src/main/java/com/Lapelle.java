package com;

import com.api.ClasseEndpoint;

import static com.utils.JsonUtil.json;
import static spark.Spark.*;

public class Lapelle {

    public static void main(String[] args) {
        servirFrontEnd();
        bindPort();

        path("/api", () -> {
            get("/classes", (request, response) -> ClasseEndpoint.recupererClasses(), json());
            get("/appelClasse/:classe", (request, response) -> ClasseEndpoint.recupererClasse(request.params(":classe")), json());
        });

        options("/*",
                (request, response) -> {
                    String accessControlRequestHeaders = request.headers("Access-Control-Request-Headers");
                    if (accessControlRequestHeaders != null) {
                        response.header("Access-Control-Allow-Headers", accessControlRequestHeaders);
                    }

                    String accessControlRequestMethod = request.headers("Access-Control-Request-Method");
                    if (accessControlRequestMethod != null) {
                        response.header("Access-Control-Allow-Methods", accessControlRequestMethod);
                    }
                    return "OK";
                });

        before((request, response) -> response.header("Access-Control-Allow-Origin", "*"));
    }

    private static void servirFrontEnd() {
        externalStaticFileLocation("frontend/dist");
    }

    private static void bindPort() {
        String portEnv = System.getenv("PORT");
        port(Integer.parseInt(portEnv));
    }
}
