package com.utils;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.services.sheets.v4.SheetsScopes;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Collections;

public class GoogleCredentialSupplier {

    public GoogleCredential getCredential() {
        String credential = System.getenv("GOOGLE_CREDENTIAL");
        InputStream credentialStream = new ByteArrayInputStream(credential.getBytes(StandardCharsets.UTF_8));
        try {
            return GoogleCredential
                    .fromStream(credentialStream)
                    .createScoped(Collections.singletonList(SheetsScopes.SPREADSHEETS_READONLY));
        } catch (IOException e) {
            throw new GoogleException(e);
        }
    }
}
