package com.utils;

import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.sheets.v4.Sheets;

import java.io.IOException;
import java.security.GeneralSecurityException;

public class Google {

    private GoogleCredentialSupplier googleCredentialSupplier;

    public Google(GoogleCredentialSupplier googleCredentialSupplier) {
        this.googleCredentialSupplier = googleCredentialSupplier;
    }

    public Sheets recupererConnecteurSheet() {
        try {
            return new Sheets.Builder(newHttpTransport(), JacksonFactory.getDefaultInstance(), googleCredentialSupplier.getCredential())
                    .setApplicationName("Google Sheet API Lapelle")
                    .build();
        } catch (GeneralSecurityException | IOException e) {
            throw new GoogleException(e);
        }
    }

    NetHttpTransport newHttpTransport() throws GeneralSecurityException, IOException {
        return GoogleNetHttpTransport.newTrustedTransport();
    }

}
