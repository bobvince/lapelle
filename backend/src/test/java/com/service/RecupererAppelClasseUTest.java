package com.service;

import com.model.*;
import com.repository.ReferentielRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.stream.Collectors;

import static com.google.common.collect.Lists.newArrayList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class RecupererAppelClasseUTest {

    @InjectMocks
    private RecupererAppelClasse recupererAppelClasse;

    @Mock
    private ReferentielRepository referentielRepository;

    @Test
    @DisplayName("execute doit retourner une partie des questions s'il y a plus de questions que d'élèves")
    void execute_DoitRecupererPartieQuestion() {
        // given
        Classe classe6X = buildClasse("6X", newArrayList("Jean", "Hulk", "Eric"));
        Classe classe6A = buildClasse("6A", newArrayList("Vincent", "Marc"));
        Classe classe3X = buildClasse("3X", newArrayList("Antoine"));

        when(referentielRepository.recupererToutLeReferentiel())
                .thenReturn(newArrayList(
                        buildReferentielNiveauAvecClasse(newArrayList(classe6X, classe6A),
                                                         newArrayList(buildQuestionReponse("A", "1"),
                                                                      buildQuestionReponse("B", "2"),
                                                                      buildQuestionReponse("C", "3"))),
                        buildReferentielNiveauAvecClasse(newArrayList(classe3X),
                                                         newArrayList(buildQuestionReponse("D", "4")))));

        // when
        AppelClasse appelClasse = recupererAppelClasse.execute("6A");

        // then
        assertThat(appelClasse.getAppelEleves())
                .extracting(AppelEleve::getQuestionReponse)
                .extracting(QuestionReponse::getQuestion)
                .hasSize(2)
                .isSubsetOf("A", "B", "C");
    }

    @Test
    @DisplayName("execute doit retourner toutes les questions s'il y autant d'élève que de question")
    void execute_DoitRecupererTouteQuestion() {
        // given
        Classe classe6X = buildClasse("6X", newArrayList("Jean", "Hulk", "Eric"));
        Classe classe6A = buildClasse("6A", newArrayList("Vincent", "Marc"));
        Classe classe3X = buildClasse("3X", newArrayList("Antoine"));

        when(referentielRepository.recupererToutLeReferentiel())
                .thenReturn(newArrayList(
                        buildReferentielNiveauAvecClasse(newArrayList(classe6X, classe6A),
                                                         newArrayList(buildQuestionReponse("A", "1"),
                                                                      buildQuestionReponse("B", "2"),
                                                                      buildQuestionReponse("C", "3"))),
                        buildReferentielNiveauAvecClasse(newArrayList(classe3X),
                                                         newArrayList(buildQuestionReponse("D", "4")))));

        // when
        AppelClasse appelClasse = recupererAppelClasse.execute("6X");

        // then
        assertThat(appelClasse.getAppelEleves())
                .extracting(AppelEleve::getQuestionReponse)
                .extracting(QuestionReponse::getQuestion)
                .containsExactlyInAnyOrder("A", "B", "C");
    }

    @Test
    @DisplayName("execute doit retourner tous les élèves d'une classe données")
    void execute_DoitRecupererTousLesEleves() {
        // given
        Classe classe6X = buildClasse("6X", newArrayList("Jean", "Hulk", "Eric"));
        Classe classe6A = buildClasse("6A", newArrayList("Vincent", "Marc"));
        Classe classe3X = buildClasse("3X", newArrayList("Antoine"));

        when(referentielRepository.recupererToutLeReferentiel())
                .thenReturn(newArrayList(
                        buildReferentielNiveauAvecClasse(newArrayList(classe6X, classe6A),
                                                         newArrayList(buildQuestionReponse("A", "1"),
                                                                      buildQuestionReponse("B", "2"),
                                                                      buildQuestionReponse("C", "3"))),
                        buildReferentielNiveauAvecClasse(newArrayList(classe3X),
                                                         newArrayList(buildQuestionReponse("D", "4")))));

        // when
        AppelClasse appelClasse = recupererAppelClasse.execute("6A");

        // then
        assertThat(appelClasse.getAppelEleves())
                .extracting(AppelEleve::getEleve)
                .extracting(Eleve::getNom)
                .containsExactlyInAnyOrder("Vincent", "Marc");
    }

    private Classe buildClasse(String nom, List<String> nomsEleve) {
        return Classe.builder()
                     .nom(nom)
                     .eleves(buildEleves(nomsEleve))
                     .build();
    }

    private List<Eleve> buildEleves(List<String> nomsEleve) {
        return nomsEleve.stream()
                        .map(eleve -> Eleve.builder().nom(eleve).build())
                        .collect(Collectors.toList());
    }

    private QuestionReponse buildQuestionReponse(String question, String reponse) {
        return QuestionReponse.builder()
                              .question(question)
                              .reponse(reponse)
                              .build();
    }

    private ReferentielNiveau buildReferentielNiveauAvecClasse(List<Classe> classes,
                                                               List<QuestionReponse> questionReponses) {
        return ReferentielNiveau.builder()
                                .classes(classes)
                                .questionReponses(questionReponses)
                                .build();
    }
}