package com.service;

import com.model.Classe;
import com.model.ReferentielNiveau;
import com.repository.ReferentielRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.stream.Collectors;

import static com.google.common.collect.Lists.newArrayList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class RecupererClassesUTest {

    @InjectMocks
    private RecupererClasses recupererClasses;

    @Mock
    private ReferentielRepository referentielRepository;

    @Test
    void execute_DoitRetournerLeNomDesClasses_RangeEnOrdreCroissant() {
        // given
        when(referentielRepository.recupererToutLeReferentiel())
                .thenReturn(newArrayList(buildAppelAvecClasse(newArrayList("6X", "6A")),
                                         buildAppelAvecClasse(newArrayList("3A", "3Z", "3F"))));

        // when
        List<String> nomDesClasses = recupererClasses.execute();

        // then
        assertThat(nomDesClasses).containsExactly("3A", "3F", "3Z", "6A", "6X");
    }

    private ReferentielNiveau buildAppelAvecClasse(List<String> nomDesClasses) {
        return ReferentielNiveau.builder()
                                .classes(buildClasses(nomDesClasses))
                                .build();
    }

    private List<Classe> buildClasses(List<String> nomDesClasses) {
        return nomDesClasses.stream()
                            .map(nom -> Classe.builder().nom(nom).build())
                            .collect(Collectors.toList());
    }
}