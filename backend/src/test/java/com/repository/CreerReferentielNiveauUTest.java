package com.repository;

import com.google.api.services.sheets.v4.model.ValueRange;
import com.model.ReferentielNiveau;
import com.model.Classe;
import com.model.Eleve;
import com.model.QuestionReponse;
import org.junit.jupiter.api.Test;

import java.util.List;

import static com.google.common.collect.Lists.newArrayList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.groups.Tuple.tuple;

class CreerReferentielsParNiveauUTest {
    @Test
    void execute_DoitRetournerUnAppel_ConstitueDeQuestionsEtReponses() {
        // Given
        CreerReferentielsParNiveau creerReferentielsParNiveau = new CreerReferentielsParNiveau();

        List<List<Object>> lignes = newArrayList(
                newArrayList("Questions", "Reponses"),
                newArrayList("Question 1", "Reponse 1"),
                newArrayList("Question 2", "Reponse 2"),
                newArrayList("Question 3", "Reponse 3")
        );

        ValueRange lignesRemplies = new ValueRange();
        lignesRemplies.setValues(lignes);

        // When
        ReferentielNiveau referentielNiveau = creerReferentielsParNiveau.execute(lignesRemplies);

        // Then
        assertThat(referentielNiveau.getQuestionReponses())
                .extracting(QuestionReponse::getQuestion, QuestionReponse::getReponse)
                .containsExactlyInAnyOrder(
                        tuple("Question 1", "Reponse 1"),
                        tuple("Question 2", "Reponse 2"),
                        tuple("Question 3", "Reponse 3"));
    }

    @Test
    void execute_DoitRetournerUnAppel_ConstitueDeClasses() {
        // Given
        CreerReferentielsParNiveau creerReferentielsParNiveau = new CreerReferentielsParNiveau();

        List<List<Object>> lignes = newArrayList(
                newArrayList("Questions", "Reponses", "3A", "3C", "3E"),
                newArrayList("", "", "", "", "")
        );

        ValueRange lignesRemplies = new ValueRange();
        lignesRemplies.setValues(lignes);

        // When
        ReferentielNiveau referentielNiveau = creerReferentielsParNiveau.execute(lignesRemplies);

        // Then
        assertThat(referentielNiveau.getClasses())
                .extracting(Classe::getNom)
                .containsExactlyInAnyOrder("3A", "3C", "3E");
    }

    @Test
    void execute_DoitRetournerUnAppel_ConstitueDeClasses_EtDEleves_AvecDesLignesDeTailleDifferentes() {
        // Given
        CreerReferentielsParNiveau creerReferentielsParNiveau = new CreerReferentielsParNiveau();

        List<List<Object>> lignes = newArrayList(
                newArrayList("Questions", "Reponses", "3A", "3C", "3E"),
                newArrayList("", "", "Jean", "Pascal"),
                newArrayList("", "", "", "Fred"),
                newArrayList("Ceci est une question", "Ceci est une réponse")
        );

        ValueRange lignesRemplies = new ValueRange();
        lignesRemplies.setValues(lignes);

        // When
        ReferentielNiveau referentielNiveau = creerReferentielsParNiveau.execute(lignesRemplies);

        // Then
        assertThat(referentielNiveau.getClasses())
                .extracting(Classe::getNom, Classe::getEleves)
                .usingRecursiveFieldByFieldElementComparator()
                .containsExactly(
                        tuple("3A", newArrayList(buildEleve("Jean"))),

                        tuple("3C", newArrayList(buildEleve("Fred"),
                                                 buildEleve("Pascal"))),

                        tuple("3E", newArrayList())
                );
    }

    @Test
    void execute_DoitRetournerUnAppel_ConstitueDeClasses_EtDElevesRangesEnOrdreAlphabetique() {
        // Given
        CreerReferentielsParNiveau creerReferentielsParNiveau = new CreerReferentielsParNiveau();

        List<List<Object>> lignes = newArrayList(
                newArrayList("Questions", "Réponses", "3A", "3C", "3E"),
                newArrayList("", "", "Jean", "Pascal", ""),
                newArrayList("", "", "", "Fred", "Vincent"),
                newArrayList("", "", "Patrick", "Steeve", "Delphine")
        );

        ValueRange lignesRemplies = new ValueRange();
        lignesRemplies.setValues(lignes);

        // When
        ReferentielNiveau referentielNiveau = creerReferentielsParNiveau.execute(lignesRemplies);

        // Then
        assertThat(referentielNiveau.getClasses())
                .extracting(Classe::getNom, Classe::getEleves)
                .usingRecursiveFieldByFieldElementComparator()
                .containsExactly(
                        tuple("3A", newArrayList(buildEleve("Jean"),
                                                 buildEleve("Patrick"))),

                        tuple("3C", newArrayList(buildEleve("Fred"),
                                                 buildEleve("Pascal"),
                                                 buildEleve("Steeve"))),

                        tuple("3E", newArrayList(buildEleve("Delphine"),
                                                 buildEleve("Vincent")))
                );
    }

    private Eleve buildEleve(String nom) {
        return Eleve.builder().nom(nom).build();
    }
}